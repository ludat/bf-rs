#[derive(Debug, PartialEq)]
pub enum Instruction {
    Loop(Vec<Instruction>),
    Increase,
    Decrease,
    PointerIncrement,
    PointerDecrement,
    Read,
    Print
}
