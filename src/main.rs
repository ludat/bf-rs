#![feature(log_syntax)]
#![feature(trace_macros)]

#[macro_use] extern crate nom;

mod instruction;
mod parsers;


use instruction::Instruction;
use std::io::Read;

struct Machine {
    tape: Vec<u8>,
    pointer: usize,
}

impl Machine {
    fn new() -> Machine {
        Machine {
            tape: vec![0],
            pointer: 0,
        }
    }

    fn execute(&mut self, program: &Vec<Instruction>) {
        use instruction::Instruction::*;
        for instruction in program {
            // println!("{}: {:?}", self.pointer, self.tape);
            match *instruction {
                Decrease => self.tape[self.pointer] -= 1,
                Increase => self.tape[self.pointer] += 1,
                PointerIncrement => {
                    self.pointer += 1;
                    self.tape.push(0);
                }
                PointerDecrement => self.pointer -= 1,
                Print => {
                    print!("{}", self.tape[self.pointer] as char);
                },
                Read => {
                    self.tape[self.pointer] = std::io::stdin().bytes().next().unwrap().unwrap();

                },
                Loop(ref program) => {
                    while self.tape[self.pointer] != 0 {
                        self.execute(&program);
                    }
                },
            }
        }
    }
}

fn main() {
    let mut machine = Machine::new();
    use nom::IResult;
    // let input = b"++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.";
    let input = b"++++[>++++++<-]>[>+++++>+++++++<<-]>>++++<[[>[[>>+<<-]<]>>>-]>-[>+>+<<-]>]+++++[>+++++++<<++>-]>.<<.";
    match parsers::program(input) {
        IResult::Done(b"", program) => {
            machine.execute(&program);
        },
        e => { println!("Parsing failed {:?}", e) },
    }
    println!("ended");
}
