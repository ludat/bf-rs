use instruction::Instruction;
use super::simple_instructions::simple_instruction;

named!(pub many_instructions ( &[ u8 ]) -> Instruction,
       map!(
           delimited!(
               char!('['),
               program,
               char!(']')
           ),
           Instruction::Loop
       )
);

named!(pub program ( &[ u8 ]) -> Vec<Instruction>,
       many0!(
           alt!(
               many_instructions | simple_instruction
           )
       )
);

#[cfg(test)]
mod tests {
    use nom::IResult::{Done, Error, Incomplete};

    use nom::Err::Position;
    use nom::ErrorKind;

    use instruction::Instruction::*;

    use super::*;
    const EMPTY: &'static [u8] = &[];

    #[test] fn a_loop_without_nothing_is_parser_correctly() {
        assert_eq!(many_instructions(b"[]"),
                   Done(EMPTY, Loop(vec![])));
    }

    #[test] fn a_loop_with_a_plus_is_parses_correctly() {
        assert_eq!(many_instructions(b"[+]"),
                   Done(EMPTY, Loop(vec![Increase])));
    }

    #[test] fn a_loop_with_two_plus_is_parses_correctly() {
        assert_eq!(many_instructions(b"[++]"),
                   Done(EMPTY, Loop(vec![Increase, Increase])));
    }
    #[test] fn a_loop_with_some_random_symbols_inside() {
        assert_eq!(many_instructions(b"[+-.,]"),
                   Done(EMPTY, Loop(vec![Increase, Decrease, Print, Read])));
    }
    #[test] fn nested_loops_are_parsed_correctly() {
        assert_eq!(many_instructions(b"[[]]"),
                   Done(EMPTY,
                        Loop(vec![
                            Loop(vec![])
                        ])
                   )
        );
    }
    // TODO this does not work
    #[ignore]
    #[test] fn ignores_not_existant_things() {
        assert_eq!(many_instructions(b"[+- this should be ignored.,]"),
                   Done(EMPTY, Loop(vec![Increase, Decrease, Print, Read])));
    }
}
