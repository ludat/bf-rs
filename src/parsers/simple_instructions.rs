use instruction::Instruction;

named!(pub increase( &[ u8 ]) -> Instruction,
       map!(
           tag!("+"),
           |_| { Instruction::Increase }
       )
);

named!(pub decrease( &[ u8 ]) -> Instruction,
       map!(
           tag!("-"),
           |_| { Instruction::Decrease }
       )
);

named!(pub pointer_increment( &[ u8 ]) -> Instruction,
       map!(
           tag!(">"),
           |_| { Instruction::PointerIncrement }
       )
);

named!(pub pointer_decrement( &[ u8 ]) -> Instruction,
       map!(
           tag!("<"),
           |_| { Instruction::PointerDecrement }
       )
);

named!(pub read( &[ u8 ]) -> Instruction,
       map!(
           tag!(","),
           |_| { Instruction::Read }
       )
);

named!(pub print( &[ u8 ]) -> Instruction,

       map!(
           tag!("."),
           |_| { Instruction::Print }
       )
);

named!(pub simple_instruction( &[ u8 ]) -> Instruction,
       alt!(
           increase | decrease |
           pointer_increment | pointer_decrement |
           read | print
       )
);

#[cfg(test)]
mod tests {
    use nom::IResult::{Done, Error, Incomplete};

    use nom::Err::Position;
    use nom::ErrorKind;

    use instruction::Instruction;

    use super::*;
    const EMPTY: &'static [u8] = &[];

    #[test] fn a_plus_should_return_an_increase_instruction() {
        assert_eq!(increase(b"+"),
                   Done(EMPTY, Instruction::Increase));
    }

    #[test] fn an_not_match_something_else() {
        assert_eq!(increase(b"j"),
                   Error(Position(ErrorKind::Tag, &b"j"[..]))
        );
    }

    #[test] fn a_minus_sign_returns_a_decrease_instruction() {
        assert_eq!(decrease(b"-"),
                   Done(EMPTY, Instruction::Decrease)
        );
    }

    #[test] fn pointer_increment_is_correct() {
        assert_eq!(pointer_increment(b">"),
                   Done(EMPTY, Instruction::PointerIncrement)
        );
    }

    #[test] fn pointer_decrement_is_correct () {
        assert_eq!(pointer_decrement(b"<"),
                   Done(EMPTY, Instruction::PointerDecrement)
        );
    }

    #[test] fn read_is_correct() {
        assert_eq!(read(b","),
                   Done(EMPTY, Instruction::Read)
        );
    }

    #[test] fn print_is_correct() {
        assert_eq!(print(b"."),
                   Done(EMPTY, Instruction::Print)
        );
    }

    #[test] fn can_parse_one_of_the_simple_instructions() {
        assert_eq!(simple_instruction(b"<"),
                   Done(EMPTY, Instruction::PointerDecrement)
        );
    }
}
